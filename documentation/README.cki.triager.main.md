# 🤖  Datawarehouse Triager 🤖

`cki.triager`

Detect and report pipeline failures.

This project aims to automatically triage common pipeline failures.
Using simple checks and regexes, datawarehouse-triager is able to
report failures on the Datawarehouse.

## Configuration

| Environment variable             | Description                                               |
|----------------------------------|-----------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT`     | Define the deployment environment (production/staging)    |
| `DATAWAREHOUSE_URL`              | URL of Datawarehouse.                                     |
| `DATAWAREHOUSE_TOKEN_TRIAGER`    | Token for Datawarehouse authentication.                   |
| `DATAWAREHOUSE_EXCHANGE_TRIAGER` | Exchange where Datawarehouse publishes objects to triage. |
| `DATAWAREHOUSE_QUEUE_TRIAGER`    | Queue to use for binding to Datawarehouse exchange.       |

### CKI_DEPLOYMENT_ENVIRONMENT

On staging developments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), the
issues found are not sent to Datawarehouse.
