# `shell-scripts/cki_tools_kernel_config_updater.sh`

Get newest stable Fedora kernel configuration files to use for upstream kernel builds.

```shell
cki_tools_kernel_config_updater.sh
```

## Environment variables

| Name                      | Secret | Required | Description                                                           |
|---------------------------|--------|----------|-----------------------------------------------------------------------|
| `BUCKET_CONFIG_NAME`      | no     | yes      | name of environment variable with the bucket specification            |
| `BUCKET_CONFIG`           | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`            |
