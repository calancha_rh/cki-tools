"""Test the matchers."""
import copy
import json
import unittest
from unittest import mock

import responses

from cki_tools.pipeline_herder import main
from cki_tools.pipeline_herder import matchers
from cki_tools.pipeline_herder import settings
from cki_tools.pipeline_herder import utils


def get_matcher_by_name(name, matchers_list=None):
    """Get a matcher from the list of matchers by name."""
    if not matchers_list:
        matchers_list = matchers.MATCHERS
    return next((m for m in matchers_list if m.name == name))


class TestChecker(unittest.TestCase):
    """Base class with Gitlab variables."""

    matcher_name = None
    user = {
        "id": 2,
    }

    project_project = {
        "id": "project",
        "path_with_namespace": "project",
    }

    pipeline_524445 = {
        "id": 524445,
        "user": {
            "id": 1,
        },
        "created_at": "2000-01-01T00:00:00",
    }

    variables_524445 = []

    jobs_524445 = [{
        "id": 764828,
        "name": "build x86_64",
        "user": {
            "id": 1,
        },
        "pipeline": {
            "id": 524445,
        },
        "created_at": "2000-01-01T00:00:01",
    }]

    job_764828 = {
        "id": 764828,
        "status": "failed",
        "stage": "build",
        "name": "build x86_64",
        "pipeline": {
            "id": 524445,
        },
        "user": {
            "id": 1,
            "username": "user",
        },
        "web_url": "https://host.name/",
    }

    artifacts_764828 = {}

    base_url = 'https://host/api/v4/projects/project'
    s3_base_url = 'https://s3-host/bucket/job-prefix'

    def mock_responses(self, s3=False):
        responses.add(responses.GET, 'https://host/api/v4/user',
                      json=self.user)
        responses.add(responses.GET, f'{self.base_url}',
                      json=self.project_project)
        responses.add(responses.GET, f'{self.base_url}/jobs/764828',
                      json=self.job_764828)
        joiner = '\n' if self.trace_764828 and isinstance(self.trace_764828[0], str) else b'\n'
        responses.add(responses.GET, f'{self.base_url}/jobs/764828/trace',
                      body=joiner.join(self.trace_764828))
        if s3:
            url = f'{self.base_url}/jobs/764828/artifacts/artifacts-meta.json'
            responses.add(responses.GET, url,
                          json.dumps({'mode': 's3', 's3_url': self.s3_base_url}))
            for name, contents in self.artifacts_764828.items():
                joiner = '\n' if contents and isinstance(contents[0], str) else b'\n'
                responses.add(responses.GET, f'{self.s3_base_url}/{name}', joiner.join(contents))
        else:
            for name, contents in self.artifacts_764828.items():
                url = f'{self.base_url}/jobs/764828/artifacts/{name}'
                joiner = '\n' if contents and isinstance(contents[0], str) else b'\n'
                responses.add(responses.GET, url, joiner.join(contents))
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445',
                      json=self.pipeline_524445)
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445/jobs?include_retried=True',
                      json=self.jobs_524445)
        responses.add(responses.GET,
                      f'{self.base_url}/pipelines/524445/variables',
                      json=self.variables_524445)

    def test_get_affected_job(self):
        """Test get_affected_job returns the checked object."""
        if not self.matcher_name:
            return
        job = mock.Mock()
        matcher = get_matcher_by_name(self.matcher_name)
        self.assertEqual(job, matcher.get_affected_job(job))


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestCode137(TestChecker):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    matcher_name = 'code137'
    trace_764828 = (
        '... snip ...',
        '%_rpmdir /builds/cki-project/cki-pipeline/workdir/rpms',
        'section_end:1586362335:build_script',
        '\x1b[0Ksection_start:1586362335:after_script',
        '\x1b[0K\x1b[0K\x1b[36;1mRunning after_script\x1b[0;m',
        '\x1b[0;msection_end:1586362335:after_script',
        '\x1b[0Ksection_start:1586362335:upload_artifacts_on_failure',
        '\x1b[0K\x1b[0K\x1b[36;1mUploading artifacts for failed job\x1b[0;m',
        '\x1b[0;msection_end:1586362335:upload_artifacts_on_failure',
        '\x1b[0K\x1b[31;1mERROR: Job failed: command terminated with exit code 137',
        '\x1b[0;m',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_exit_137(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137)."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            mock.ANY, get_matcher_by_name('code137'), 0)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_exit_137_non_utf8(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137) with broken encoding."""
        self.trace_764828 = (
            b'\xff ERROR: Job failed: command terminated with exit code 137',
        )
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        submit_retry.assert_called_once_with(
            mock.ANY, get_matcher_by_name('code137'), 0)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestForcePush(TestChecker):
    """Test the detection of force push on merge jobs."""

    matcher_name = 'force-push'
    trace_764828 = (
        '... snip ...',
        'See "git help gc" for manual housekeeping.',
        'Successfully rebased and updated refs/heads/queue/5.7.',
        'fatal: reference is not a tree: d6d92c1d',
        '202e1926e06333ee3dee9fe2c1b5ed78',
        'Running after_script',
        '00:01',
        'Running after script...',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_force_push(self, submit_retry: mock.MagicMock):
        """Test force push checks."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestFallback(TestChecker):
    """Test the detection of force push on merge jobs."""

    matcher_name = 'force-push'
    trace_764828 = (
        'Something failed',
    )
    matchers = [
        utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            action='something-unhandled',
        )
    ]

    @responses.activate
    @mock.patch.object(main.matchers, 'MATCHERS', matchers)
    def test_fallback(self):
        """Test fall backing to error action if action is not handled."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'error')


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestTooLarge(TestChecker):
    """Test the detection of artifacts size exceeded."""

    matcher_name = 'too-large'
    trace_764828 = (
        '... snip ...',
        'Uploading artifacts for failed job',
        'Uploading artifacts...',
        'artifacts: found 2865 matching files',
        'ERROR: Uploading artifacts to coordinator... too large archive  ',
        'id=931289 responseStatus=413 Request Entity Too Large status=413 ',
        'Request Entity Too Large token=s216cQxY',
        'FATAL: too large',
        'ERROR: Job failed: exit code 2',
    )

    @responses.activate
    def test_artifacts_size(self):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestGitCache(TestChecker):
    """Test the detection of missing git-caches."""

    matcher_name = 'missing-git-cache'
    trace_764828 = (
        '... snip ...',
        'download failed: s3://cki/git-cache/torvalds.linux.tar to - An error'
        ' occurred (NoSuchKey) when calling the GetObject operation: Unknown',
        'tar: This does not look like a tar archive',
        'tar: Exiting with failure status due to previous errors',
        '... snip ...',
        'ERROR: Job failed: command terminated with exit code 1',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_git_cache(self, submit_retry: mock.MagicMock):
        """Test the detection of artifact size exceeded."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestArtifactsMatching(TestChecker):
    """Test the detection of missing git-caches."""

    matcher_name = 'process-limit'
    trace_764828 = (
        'Something failed',
    )

    artifacts_764828 = {'artifacts/build.log': (
        '00:01:18 /bin/sh: fork: Resource temporarily unavailable',
        '00:01:19   LD [M]  drivers/gpu/drm/vmwgfx/vmwgfx.o',
        '00:01:19 make[3]: *** [scripts/Makefile.build:500: fs] Error 2',
        '... snip ...',
        '00:01:21 make: *** [Makefile:1523: targz-pkg] Error 2',
    )}

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_match(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific jobs."""
        self.mock_responses()

        mock_matchers = [utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            job_name='build',
        )]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_nomatch(self, submit_retry: mock.MagicMock):
        """Test the detection if the job name differs."""
        self.mock_responses()

        mock_matchers = [utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            job_name='test',
        )]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_artifact(self, submit_retry: mock.MagicMock):
        """Test the detection in the job artifacts."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_artifact_non_utf8(self, submit_retry: mock.MagicMock):
        """Test the detection in the job artifacts with encoding errors."""
        self.artifacts_764828 = {'artifacts/build.log': (
            b'\xff fork: Resource temporarily unavailable',
        )}
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_artifact_s3(self, submit_retry: mock.MagicMock):
        """Test the detection in the S3 job artifacts."""
        self.mock_responses(s3=True)

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_job_status(self, submit_retry: mock.MagicMock):
        """Test the detection only for specific job states."""
        self.mock_responses()

        mock_matchers = [utils.Matcher(
            name='error-dummy',
            description='Dummy error',
            messages=['Something failed'],
            job_status=('passed',)
        )]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)
            self.assertEqual(action, None)
            mock_matchers[0].job_status = ('passed', 'failed')
            action = main.process_job('host', 'project', 764828)
            self.assertEqual(action, 'retry')
            self.assertTrue(submit_retry.called)


class MatcherMock:
    # pylint: disable=too-few-public-methods
    """Mock Matcher."""

    def __init__(self, return_value=False):
        """Init."""
        self.check = mock.Mock(return_value=return_value)


class TestFunctions(unittest.TestCase):
    """Test matchers functions."""

    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS',
                [MatcherMock(), MatcherMock(), MatcherMock()])
    def test_match_checks_all(self):
        """Test match() iterates over all the matchers."""

        result = matchers.match('foobar')

        self.assertIsNone(result)
        for matcher in matchers.MATCHERS:
            matcher.check.assert_called_with('foobar')

    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS',
                [MatcherMock(), MatcherMock(return_value=True), MatcherMock(return_value=True)])
    def test_match_return(self):
        """Test match() returns the first match."""
        result = matchers.match('foobar')
        self.assertEqual(result, matchers.MATCHERS[1])


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestDependingJobsMatcher(unittest.TestCase):
    """Test the detection of depending jobs."""

    user = {"id": 2, "username": "user"}
    project_project = {"id": "project", "path_with_namespace": "project"}

    pipeline_524445 = {"id": 524445, "user": {"id": 1}, "created_at": "2000-01-01T00:00:00"}
    variables_524445 = []

    jobs_524445 = [
        {"id": 764827, "name": "build x86_64", "user": user, "pipeline": pipeline_524445,
         "created_at": "2000-01-01T00:00:01", "status": "failed"},
        {"id": 764828, "name": "build x86_64", "user": user, "pipeline": pipeline_524445,
         "created_at": "2000-01-01T00:00:02"},
        {"id": 764829, "name": "publish x86_64", "user": user, "pipeline": pipeline_524445,
         "created_at": "2000-01-01T00:00:03"},
    ]

    job_764827 = {
        "id": 764828, "status": "failed", "stage": "build", "name": "build x86_64",
        "pipeline": pipeline_524445, "user": user, "web_url": "https://host.name/",
    }
    job_764828 = {
        "id": 764828, "status": "success", "stage": "build", "name": "build x86_64",
        "pipeline": pipeline_524445, "user": user, "web_url": "https://host.name/",
    }
    job_764829 = {
        "id": 764829, "status": "success", "stage": "publish", "name": "publish x86_64",
        "pipeline": pipeline_524445, "user": user, "web_url": "https://host.name/",
    }

    base_url = 'https://host/api/v4/projects/project'

    def mock_responses(self):
        responses.add(responses.GET, 'https://host/api/v4/user',
                      json=self.user)
        responses.add(responses.GET, f'{self.base_url}',
                      json=self.project_project)
        responses.add(responses.GET, f'{self.base_url}/jobs/764827',
                      json=self.job_764827)
        responses.add(responses.GET, f'{self.base_url}/jobs/764828',
                      json=self.job_764828)
        responses.add(responses.GET, f'{self.base_url}/jobs/764829',
                      json=self.job_764829)
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445',
                      json=self.pipeline_524445)
        responses.add(responses.GET, f'{self.base_url}/pipelines/524445/jobs?include_retried=True',
                      json=self.jobs_524445)

    def test_init(self):
        """Test init parameters assign."""
        matcher = utils.DependingJobsMatcher(
            'job_name', 'depending_job_name',
            job_status='job_status', action='action'
        )
        self.assertEqual('job_name', matcher.job_name)
        self.assertEqual('depending_job_name', matcher.depending_job_name)
        self.assertEqual('job_status', matcher.job_status)
        self.assertEqual('action', matcher.action)
        self.assertEqual('job_name->depending_job_name (job_status)', matcher.name)
        self.assertEqual('job_name->depending_job_name (job_status)', matcher.description)

    def test_init_default(self):
        """Test init default parameters assign."""
        matcher = utils.DependingJobsMatcher(
            'job_name', 'depending_job_name',
        )
        self.assertEqual('success', matcher.job_status)
        self.assertEqual('retry', matcher.action)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS',
                utils.DependingJobsMatcher.from_list([
                    (['build s390x', 'build_tools s390x'], 'publish s390x'),
                    ('build x86_64', 'publish x86_64')
                ]))
    def test_get_affected_job(self):
        """Test get_affected_job returns the correct job."""
        self.mock_responses()

        matcher = get_matcher_by_name('build x86_64->publish x86_64 (success)')
        build_job = utils.CachedJob("host", "project", 764828)
        publish_job = utils.CachedJob("host", "project", 764829)

        affected_job = matcher.get_affected_job(build_job)
        self.assertEqual(publish_job.gl_job.id, affected_job.gl_job.id)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'foobar')
    ])
    def test_get_affected_job_not_found(self):
        """Test get_affected_job when it can't find the job."""
        self.mock_responses()

        matcher = get_matcher_by_name('build x86_64->foobar (success)')
        build_job = utils.CachedJob("host", "project", 764828)

        self.assertEqual(matcher.get_affected_job(build_job), None)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('foo', 'publish x86_64')
    ])
    def test_check_different_name(self):
        """Test check() compares the job name."""
        self.mock_responses()
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)
        self.assertFalse(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64', job_status='fail')
    ])
    def test_check_different_status(self):
        """Test check() compares the job status."""
        self.mock_responses()
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)
        self.assertFalse(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64')
    ])
    def test_check_ignored_status(self):
        """Test check() with depending jobs on not-run statuses."""
        ignored_statuses = ['canceled', 'created', 'pending']
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        for status in ignored_statuses:
            responses.reset()
            self.job_764829['status'] = status
            self.mock_responses()
            self.assertFalse(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64')
    ])
    def test_check_running_new(self):
        """Test check() with depending job already running. Just started."""
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        responses.reset()
        self.job_764828['finished_at'] = '2021-03-01T00:00:00.000Z'
        self.job_764829['status'] = 'running'
        self.job_764829['created_at'] = '2021-03-01T00:00:01.000Z'
        self.mock_responses()

        self.assertFalse(matcher.check(build_job))

    @responses.activate
    def test_check_none_failed(self):
        """Test check() with no failed jobs."""
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        responses.reset()
        self.job_764827['status'] = 'success'
        self.mock_responses()

        self.assertFalse(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64')
    ])
    def test_check_running_old(self):
        """Test check() with depending job already running. Started some time ago."""
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        responses.reset()
        self.job_764828['finished_at'] = '2021-03-01T00:00:01.000Z'
        self.job_764829['status'] = 'running'
        self.job_764829['created_at'] = '2021-03-01T00:00:00.000Z'
        self.mock_responses()

        self.assertTrue(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64')
    ])
    def test_check_finished_status(self):
        """Test check() with depending jobs on finished statuses."""
        finished_statuses = ['success', 'failed']
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        for status in finished_statuses:
            responses.reset()
            self.job_764829['status'] = status
            self.mock_responses()
            self.assertTrue(matcher.check(build_job))

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.matchers.MATCHERS', [
        utils.DependingJobsMatcher('build x86_64', 'publish x86_64')
    ])
    def test_check_unhandled_status(self):
        """Test check() with depending jobs on unhandled statuses."""
        ignored_statuses = ['manual', 'skipped']
        matcher = matchers.MATCHERS[0]
        build_job = utils.CachedJob("host", "project", 764828)

        for status in ignored_statuses:
            responses.reset()
            self.job_764829['status'] = status
            self.mock_responses()
            self.assertFalse(matcher.check(build_job))

    def test_from_list(self):
        """Test from_list class method."""
        rules_list = [
            (['job_1', 'job_2'], 'job_3'),
            ('job_4', 'job_5'),
        ]
        expected_rules = [
            ('job_1', 'job_3'),
            ('job_2', 'job_3'),
            ('job_4', 'job_5'),
        ]

        rules = utils.DependingJobsMatcher.from_list(rules_list)
        self.assertTrue(isinstance(rules, list))
        for job, dependency in expected_rules:
            get_matcher_by_name(f'{job}->{dependency} (success)', matchers_list=rules)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestNotifications(TestChecker):
    """Test the notifications for non-failures."""

    matcher_name = 'beaker-down'
    trace_764828 = (
        'WARNING XML-RPC connection to beaker.engineering.redhat.com failed',
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    def test_ok(self, notify_finished):
        """Check that the job is notified when it's all good."""
        self.mock_responses()

        mock_matchers = [
            utils.Matcher(
                name='error-dummy',
                description='Dummy error',
                messages=['all is ok'],
            )
        ]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        # Check that no matcher matched
        self.assertEqual(None, action)

        # So notify_finished was called
        self.assertTrue(notify_finished.called)
        notify_finished.assert_called_with('host', 'project', 764828)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    def test_nok(self, notify_finished):
        """Check that the job is not notified when the matcher found a problem."""
        self.mock_responses()

        mock_matchers = [
            utils.Matcher(
                name='error-dummy',
                description='Dummy error',
                messages=['WARNING XML-RPC'],
            )
        ]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        # Check that the matcher matched
        self.assertEqual('retry', action)

        # And it didn't call notify_finished
        self.assertFalse(notify_finished.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry', mock.MagicMock())
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished')
    def test_always_notify(self, notify_finished):
        """Check that the job is notified if matcher.always_notify = True."""
        self.mock_responses()

        mock_matchers = [
            utils.Matcher(
                name='error-dummy',
                description='Dummy error',
                messages=['WARNING XML-RPC'],
                always_notify=True
            )
        ]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        # Check that the matcher matched
        self.assertEqual('retry', action)

        # But it called notify_finished anyway because of always_notify==True
        self.assertTrue(notify_finished.called)
        notify_finished.assert_called_with('host', 'project', 764828)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestNoTrace(TestChecker):
    """Test the detection of jobs without trace."""

    matcher_name = 'no-trace'
    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_trace(self, submit_retry: mock.MagicMock):
        """Test job without trace."""
        self.mock_responses()

        mock_matchers = [utils.NoTraceMatcher()]
        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_trace_ok(self, submit_retry: mock.MagicMock):
        """Test job with trace."""
        self.trace_764828 = ('foobar',)
        self.mock_responses()

        mock_matchers = [utils.NoTraceMatcher()]
        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
class TestTestsNotRun(TestChecker):
    """Test the detection of tests not run."""

    matcher_name = 'tests-not-run'
    trace_764828 = (
        'Everything finished ok.'
    )

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_ok(self, submit_retry: mock.MagicMock):
        """Check that all tests are ok."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "rh:1", "origin": "redhat", "build_id": "rh:1", "status": "SKIP"},'
                '    {"id": "rh:2", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        mock_matchers = [utils.TestsNotRun()]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertEqual(action, None)
        self.assertFalse(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_not_result(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that at least a test has no results."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "rh:1", "origin": "redhat", "build_id": "rh:1"},'
                '    {"id": "rh:2", "origin": "redhat", "build_id": "rh:1"},'
                '    {"id": "rh:3", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        mock_matchers = [utils.TestsNotRun()]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)
        notify.assert_called_with(mock.ANY, 'Detected 2 test(s) that did not run', True)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_no_kcidb(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that kcidb file can be missing."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.mock_responses()

        mock_matchers = [utils.TestsNotRun()]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)
        notify.assert_not_called()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    def test_forced_skip_status(self, notify: mock.MagicMock, submit_retry: mock.MagicMock):
        """Check that at least a test has forced_skip_status."""
        self.job_764828['stage'] = 'test'
        self.job_764828['name'] = 'test x86_64'
        self.artifacts_764828 = {
            'kcidb_all.json': [
                '{'
                '  "version": {"major": 4, "minor": 0},'
                '  "tests": ['
                '    {"id": "rh:1", "origin": "redhat", "build_id": "rh:1", "status": "SKIP",'
                '     "misc": {"forced_skip_status": true}},'
                '    {"id": "rh:2", "origin": "redhat", "build_id": "rh:1", "status": "SKIP"},'
                '    {"id": "rh:3", "origin": "redhat", "build_id": "rh:1", "status": "PASS"}'
                '  ]'
                '}'
            ]
        }
        self.mock_responses()

        mock_matchers = [utils.TestsNotRun()]

        with mock.patch.object(main.matchers, 'MATCHERS', mock_matchers):
            action = main.process_job('host', 'project', 764828)

        self.assertEqual(action, 'report')
        self.assertFalse(submit_retry.called)
        notify.assert_called_with(mock.ANY, 'Detected 1 test(s) that did not run', True)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(main.matchers, 'MATCHERS', [utils.DataIntegrityMatcher()])
class TestDataIntegrityMatcher(TestChecker):
    """Test the detection of data_integrity_failure."""

    matcher_name = 'integrity'
    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_failure(self, submit_retry: mock.MagicMock):
        """Test the detection."""
        self.job_764828 = copy.deepcopy(self.job_764828)
        self.job_764828['failure_reason'] = 'data_integrity_failure'
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_failure_reason(self, submit_retry: mock.MagicMock):
        """Test that a non-existing failure reason is ok."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)


@mock.patch.dict('os.environ', {'URL_SHORTENER_URL': ''})
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
@mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
@mock.patch.object(main.matchers, 'MATCHERS', [utils.TimeoutMatcher()])
class TestTimeout(TestChecker):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    matcher_name = 'timeout'
    trace_764828 = ()

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_timeout(self, submit_retry: mock.MagicMock):
        """Test the detection of stuck jobs."""
        self.job_764828 = copy.deepcopy(self.job_764828)
        self.job_764828['failure_reason'] = 'stuck_or_timeout_failure'
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertEqual(action, 'retry')
        self.assertTrue(submit_retry.called)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    def test_no_failure_reason(self, submit_retry: mock.MagicMock):
        """Test that a non-existing failure reason is ok."""
        self.mock_responses()

        action = main.process_job('host', 'project', 764828)
        self.assertIsNone(action)
        self.assertFalse(submit_retry.called)
