"""Tests for OSCI gating messages."""
from importlib import resources
import json
from pathlib import Path
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time

from cki_tools import gating_reporter

from . import assets
from .utils import b64decode_and_decompress


class TestGating(unittest.TestCase):
    """Tests for OSCI gating messages."""

    def test_compress_and_b64encode(self):
        """Test for compress_and_b64encode."""
        test_cases = ['foo', 'bar', 'baz', '123']
        for data in test_cases:
            result = gating_reporter.compress_and_b64encode(data)
            self.assertTrue(isinstance(result, str))
            self.assertEqual(b64decode_and_decompress(result), data)

    def test_process_message(self):
        """Verify ready_to_report messages are handled correctly."""
        cases = (
            ('ready_to_report', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, True),
            ('build_setups_finished', {
                'status': 'build_setups_finished',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, True),
            ('unknown status', {
                'status': 'foo',
                'object_type': 'checkout',
                'id': 'redhat:123',
            }, False),
            ('no checkout', {
                'status': 'ready_to_report',
                'object_type': 'build',
                'id': 'redhat:15',
            }, False),
            ('retriggered', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'id': 'redhat:123',
                'object': {'data': 'something', 'misc': {'retrigger': True}},
            }, False),
        )

        for description, payload, reports in cases:
            with (self.subTest(description),
                  mock.patch('cki_tools.gating_reporter.report') as mock_report):
                gating_reporter.process_message(body=payload)
                if reports:
                    mock_report.assert_called()
                else:
                    mock_report.assert_not_called()

    def _test(self, issue=False, regression=False, **kwargs):
        test = mock.Mock(**kwargs)
        test.issues_occurrences.list.return_value = []
        if issue:
            test.issues_occurrences.list.return_value.append(mock.Mock(is_regression=regression))
        return test

    def _checkout(self, builds=None, **kwargs):
        checkout = mock.Mock(**kwargs)
        checkout.builds.list.return_value = builds or []
        checkout.manager.api.host = 'https://host'
        return checkout

    def _build(self, tests=None, **kwargs):
        build = mock.Mock(**kwargs)
        build.tests.list.return_value = tests or []
        return build

    def test_unknown_issues(self):
        """Verify unknown issues are filtered correctly."""
        cases = (
            ('fail', 'FAIL', 1,
                {'status': 'FAIL', 'waived': False}),
            ('pass', 'FAIL', 0,
                {'status': 'PASS', 'waived': False}),
            ('error', 'FAIL', 0,
                {'status': 'ERROR', 'waived': False}),
            ('waived', 'FAIL', 0,
                {'status': 'FAIL', 'waived': True}),
            ('issue', 'FAIL', 0,
                {'status': 'FAIL', 'waived': False, 'issue': True}),
            ('regression', 'FAIL', 1,
                {'status': 'FAIL', 'waived': False, 'issue': True, 'regression': True}),
        )

        for description, status, expected, payload in cases:
            with self.subTest(description):
                self.assertEqual(len(gating_reporter.unknown_issues(
                    [self._test(**payload)], status)), expected)

    def test_make_xunit_string(self):
        """Test for make_xunit_string."""
        self.maxDiff = None
        assets_dir = Path(__file__).resolve().parent / "assets"

        with self.subTest("Return None if no tests"):
            self.assertIsNone(gating_reporter.make_xunit_string([]))

        # Mock the kcidb tests
        with open(assets_dir / "test_results.json", encoding='utf-8') as file:
            kcidb_tests_mocks = [
                mock.MagicMock(**test)
                for test in json.load(file)
            ]
        # Expected xunit XML
        with open(assets_dir / "expected_xunit.xml", encoding='utf-8') as file:
            expected_xunit_xml = file.read().strip().replace('\t', '').replace('\n', '')

        xunit_data = gating_reporter.make_xunit_string(kcidb_tests_mocks)
        xunit_xml = b64decode_and_decompress(xunit_data).strip().replace('\n', '')

        # Main test case
        with self.subTest("Test generated xunit XML matches expected"):
            self.assertEqual(xunit_xml, expected_xunit_xml)

        # Subtest for the case when `make_xunit_string` fails
        with self.subTest("Test make_xunit_string fails and logs error"):
            with self.assertLogs(gating_reporter.LOGGER, 'ERROR') as log:
                xunit_data = gating_reporter.make_xunit_string('foo')
                self.assertIn('An error occurred while generating the xunit string',
                              log.output[-1])
                self.assertIsNone(xunit_data)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    def test_gating_messages(self):
        """Tests for the actual messages."""

        cases = (
            ('complete', 'complete', 'osci-complete.yml', 123456),
            ('no task id', 'complete', None, None),
            ('running', 'running', 'osci-running.yml', 123456),
        )

        for description, message_type, asset, brew_task_id in cases:
            builds = [
                self._build(
                    architecture='x86_64', id='redhat:11', misc={
                        'iid': 12347, 'package_name': 'kernel',
                        'kpet_tree_family': 'rhel'}),
                self._build(
                    architecture='s390x', id='redhat:10', misc={
                        'iid': 12346, 'package_name': 'kernel-rt',
                        'kpet_tree_family': 'rhel', 'debug': True},
                    tests=[self._test(status='FAIL', waived=False, issue=True, regression=True)]),
                self._build(
                    architecture='zzz', id='redhat:15', misc={
                        'iid': 12346, 'package_name': 'kernel-rt',
                        'kpet_tree_family': 'rhel-rt', 'debug': False},
                    tests=[self._test(status='PASS', waived=False)]),
                self._build(
                    architecture='x86_64', id='redhat:12', misc={
                        'iid': 12348, 'package_name': 'kernel',
                        'kpet_tree_family': 'rhel-rt', 'debug': True},
                    tests=[self._test(status='ERROR', waived=False)]),
            ]
            checkout = self._checkout(
                id='redhat:checkout',
                misc={'scratch': False,
                      'kernel_version': '123.notatest',
                      'source_package_name': 'source-package-name',
                      'iid': 1112,
                      **({'brew_task_id': brew_task_id} if brew_task_id is not None else {})},
                attributes={'contacts': ['joe@example.email']},
                builds=builds)

            with (self.subTest(description),
                  mock.patch('cki_lib.stomp.StompClient.send_message') as send_message,
                  mock.patch('cki_tools.gating_reporter.dw_checkout', return_value=checkout) as dw,
                  mock.patch('cki_tools.gating_reporter.make_xunit_string',
                             return_value='mockedbase64payload')):
                gating_reporter.report(message_type, 'checkout_id')
                dw.assert_called_once_with('checkout_id')
                messages = [c.args[0] for c in send_message.mock_calls]
                if asset is not None:
                    self.assertEqual(messages, yaml.load(
                        contents=resources.files(assets).joinpath(asset).read_text('utf8')))
                else:
                    self.assertEqual(messages, [])
