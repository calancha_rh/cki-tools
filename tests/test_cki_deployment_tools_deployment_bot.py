"""Test cki.deployment_tools.deployment_bot."""
import os
import tempfile
import unittest
from unittest import mock

from cki_lib import config_tree
import gitlab
import responses
import yaml

from cki.deployment_tools.deployment_bot import __main__
from cki.deployment_tools.deployment_bot import deployment
from cki.deployment_tools.deployment_bot import matcher
from cki.deployment_tools.deployment_bot import utils

PROJECT_CONFIG = {
    '.single': {
        '.deployment': {
            'project_url': 'https://deployment-instance/project',
            'ref': 'main',
        },
        'mr-start': {
            'trigger': {
                'object_kind': 'build',
                'build_status': 'success',
                'build_id': '(?P<job_id>.*)',
                'environment': {
                    'action': 'start',
                    'name': '(?P<mr>mr-[0-9]+)',
                },
            },
            'deployment': {
                'message': {
                    'success': '{path_with_namespace} {commit_message} {environment} {job_url} ok',
                    'failed': '{path_with_namespace} {commit_message} {environment} {job_url} fail'
                },
                'variables': {
                    'JOB': '{job_id}',
                    'NAME': '{project}',
                    'PROJECT_PATH': '{path}',
                    'TAG': '{mr}',
                    'FOO': '{foo}',
                },
            },
        },
    },
    'irc-bot': {
        '.extends': '.single',
        '.trigger': {
            'project_url': 'https://source-project-url/group/(?P<project>name)',
            'captures': {
                'foo': 'baz',
            },
        },
    },
}


class TestUtils(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test utils."""

    def test_parse_project_url(self):
        """Check that project URL parsing is working as expected."""
        self.assertEqual(
            utils.parse_project_url('https://instance/group/project'),
            ('https://instance', 'group/project'))

    def test_parse_job_url(self):
        """Check that job URL parsing is working as expected."""
        self.assertEqual(
            utils.parse_job_url('https://instance/group/project/-/jobs/1234'),
            ('https://instance', 'group/project', 1234))

    def test_parse_environment_url(self):
        """Check that environment URL parsing is working as expected."""
        self.assertEqual(
            utils.parse_environment_url('https://instance/name/-/environments/1'),
            ('https://instance', 'name', 1))


class TestMatcher(unittest.TestCase):
    """Test Matcher."""

    @responses.activate
    def _match(self, config, extra_variables=None, env=True):
        api = 'https://source-project-url/api/v4/projects'
        if env:
            responses.add(responses.GET,
                          f'{api}/group%2Fname/environments?states=available',
                          json=[{'id': 1}])
            responses.add(responses.GET,
                          f'{api}/group%2Fname/environments/1',
                          json={'id': 1, 'name': 'env',
                                'last_deployment': {'deployable': {'id': 1}}})
        else:
            responses.add(responses.GET,
                          f'{api}/group%2Fname/environments?states=available',
                          json=[])
        responses.add(responses.GET,
                      f'{api}/group%2Fname/jobs/1',
                      json={'ref': 'main', 'commit': {'id': 2, 'title': 'commit title'}})
        responses.add(responses.GET,
                      f'{api}/group%2Fname/repository/commits/2/merge_requests',
                      json=[])

        self.assertEqual(matcher.Matcher(
            config,
            'https://source-project-url/group/name',
            {
                'object_kind': 'build',
                'build_status': 'success',
                'build_id': 1,
                'environment': {
                    'action': 'start',
                    'name': 'mr-123',
                },
            }).match(), {
                'JOB': '1',
                'NAME': 'name',
                'PROJECT_PATH': 'name',
                'TAG': 'mr-123',
                'FOO': 'baz',
                'deployment_bot_message_success': 'group/name commit title env '
                'https://source-project-url/group/name/-/jobs/1 ok',
                'deployment_bot_message_failed': 'group/name commit title env '
                'https://source-project-url/group/name/-/jobs/1 fail',
                **({
                    'deployment_bot_environment_url':
                    'https://source-project-url/group/name/-/environments/1',
                    'deployment_bot_message_success': 'group/name commit title env '
                    'https://source-project-url/group/name/-/jobs/1 ok',
                    'deployment_bot_message_failed': 'group/name commit title env '
                    'https://source-project-url/group/name/-/jobs/1 fail',
                } if env else {
                    'deployment_bot_message_success': 'group/name commit title unknown '
                    'https://source-project-url/group/name/-/jobs/1 ok',
                    'deployment_bot_message_failed': 'group/name commit title unknown '
                    'https://source-project-url/group/name/-/jobs/1 fail',
                }),
                **(extra_variables or {}),
        })

    def test_match(self):
        """Check that matching and variable replacement works."""
        self._match(config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot'])

    def test_match_env_from_project_config(self):
        """Check that passing the environment variable via the project works."""
        config = config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot']
        config['.deployment']['external_url'] = 'dotenv:dotenv-file:variable-name'
        self._match(config, {
            'deployment_bot_external_url': 'dotenv:dotenv-file:variable-name', })

    def test_match_env_from_deployment_config(self):
        """Check that passing the environment variable via the deployment works."""
        config = config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot']
        config['mr-start']['deployment']['external_url'] = 'dotenv:dotenv-file:variable-name'
        self._match(config, {'deployment_bot_external_url': 'dotenv:dotenv-file:variable-name'})

    def test_match_no_env(self):
        """Check that passing the environment variable via the deployment works."""
        config = config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot']
        config['mr-start']['deployment']['external_url'] = 'dotenv:dotenv-file:variable-name'
        self._match(config, {'deployment_bot_external_url': 'dotenv:dotenv-file:variable-name'},
                    env=False)

    def test_no_match_project_url(self):
        """Check that matching fails if the project url differs."""
        self.assertEqual(matcher.Matcher(
            config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot'],
            'https://source-project-url/group/wrong-name',
            {
                'object_kind': 'build',
                'build_status': 'success',
                'build_id': 12345,
                'environment': {
                    'action': 'start',
                    'name': 'mr-123',
                },
            }).match(), None)

    def test_no_match_message(self):
        """Check that matching fails if the message differs."""
        self.assertEqual(matcher.Matcher(
            config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot'],
            'https://source-project-url/group/name',
            {
                'object_kind': 'build',
                'build_status': 'failed',
                'build_id': 12345,
                'environment': {
                    'action': 'start',
                    'name': 'mr-123',
                },
            }).match(), None)

    def test_no_match_message_deep(self):
        """Check that matching fails if the message differs further down."""
        self.assertEqual(matcher.Matcher(
            config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot'],
            'https://source-project-url/group/name',
            {
                'object_kind': 'build',
                'build_status': 'success',
                'build_id': 12345,
                'environment': {
                    'action': 'stop',
                    'name': 'mr-123',
                },
            }).match(), None)

    def test_no_match_message_missing(self):
        """Check that matching fails if some part of the message is missing."""
        self.assertEqual(matcher.Matcher(
            config_tree.process_config_tree(PROJECT_CONFIG)['irc-bot'],
            'https://source-project-url/group/name',
            {
                'object_kind': 'build',
                'build_status': 'success',
                'build_id': 12345,
                'environment': {
                    'name': 'mr-123',
                },
            }).match(), None)

    @responses.activate
    def test_commit_message_mr(self):
        """Check generation of a nice message from an MR pipeline."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/1',
                      json={'ref': 'refs/merge-requests/2/merge'})
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/merge_requests/2',
                      json={'title': 'MR title'})
        gl_instance = gitlab.Gitlab('https://instance')
        gl_project = gl_instance.projects.get('group/project', lazy=True)
        gl_job = gl_project.jobs.get(1)
        self.assertEqual(matcher.Matcher.commit_message(gl_project, gl_job),
                         'MR title')

    @responses.activate
    def test_commit_message_commit_mr(self):
        """Check generation of a nice message from a merged commit."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/1',
                      json={'ref': 'main', 'commit': {'id': 2}})
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/repository/commits/2/merge_requests',
                      json=[{'title': 'MR title'}])
        gl_instance = gitlab.Gitlab('https://instance')
        gl_project = gl_instance.projects.get('group/project', lazy=True)
        gl_job = gl_project.jobs.get(1)
        self.assertEqual(matcher.Matcher.commit_message(gl_project, gl_job),
                         'MR title')

    @responses.activate
    def test_commit_message_commit_title(self):
        """Check generation of a nice message from a commit."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/1',
                      json={'ref': 'main', 'commit': {'id': 2, 'title': 'commit title'}})
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/repository/commits/2/merge_requests',
                      json=[])
        gl_instance = gitlab.Gitlab('https://instance')
        gl_project = gl_instance.projects.get('group/project', lazy=True)
        gl_job = gl_project.jobs.get(1)
        self.assertEqual(matcher.Matcher.commit_message(gl_project, gl_job),
                         'commit title')


class TestDeployment(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test Deployment."""

    @staticmethod
    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_set_environment_url_non_prod():
        """Check setting the external URL of an environment is not done for non-prod."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/environments/1',
                      json={'id': 1, 'last_deployment': {'deployable': {'id': 1234}}})
        deployment.Deployment.set_environment_external_url(
            'https://instance/group/project/-/environments/1', 'env-url')

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_set_environment_url_prod(self):
        """Check setting the external URL of an environment is not done for non-prod."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/environments/1',
                      json={'id': 1, 'last_deployment': {'deployable': {'id': 1234}}})
        responses.add(responses.PUT,
                      'https://instance/api/v4/projects/group%2Fproject/environments/1',
                      json={})

        deployment.Deployment.set_environment_external_url(
            'https://instance/group/project/-/environments/1', 'env-url')
        self.assertEqual(responses.calls[-1].request.body, b'{"external_url": "env-url"}')

    @responses.activate
    def _dotenv(self, key, value):
        """Check reading dotenv settings."""
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/1/artifacts/dot.env',
                      status=404)
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/2/artifacts/dot.env',
                      body='key2=value2\nkey=value\n')
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/3/artifacts/dot.env',
                      body='key3=value3\n')
        self.assertEqual(deployment.Deployment.dotenv('https://instance/group/project',
                                                      [1, 2, 3], 'dot.env', key), value)

    @responses.activate
    def test_dotenv(self):
        """Test that dotenv files are read."""
        self._dotenv('key', 'value')

    @responses.activate
    def test_dotenv_later(self):
        """Test that dotenv files are read until a certain variable is found."""
        self._dotenv('key3', 'value3')

    @responses.activate
    def test_dotenv_not_found(self):
        """Test that missing variables from dotenv files are returned as None."""
        self._dotenv('key4', None)

    @responses.activate
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.dotenv',
                mock.Mock(return_value='env-url'))
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.'
                'Deployment.set_environment_external_url')
    @mock.patch('cki_lib.messagequeue.MessageQueue.send_message')
    def _handle_deployment_pipeline_update(self, send_message, set_environment_external_url, *,
                                           is_production=False, variables=None, status='success',
                                           expected_result=True, env_url='env-url'):
        # pylint: disable=too-many-arguments
        api = 'https://instance/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/jobs/1',
                      json={'ref': 'main', 'commit': {'id': 2, 'title': 'commit title'}})
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/repository/commits/2/merge_requests',
                      json=[])
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/environments?states=available',
                      json=[{'id': 1}])
        responses.add(responses.GET,
                      f'{api}/group%2Fproject/environments/1',
                      json={'id': 1, 'name': 'env', 'last_deployment': {'deployable': {'id': 1}}})
        result = deployment.Deployment(PROJECT_CONFIG).handle_deployment_pipeline_update(
            'https://instance/deployment',
            [1, 2, 3],
            status,
            variables or {'deployment_bot_environment_url':
                          'https://instance/group/name/-/environments/1',
                          'deployment_bot_external_url': 'dotenv:dotenv-file:variable-name',
                          'deployment_bot_message_success': 'message'})
        self.assertEqual(result, expected_result)
        if expected_result:
            if env_url:
                set_environment_external_url.assert_called_with(
                    'https://instance/group/name/-/environments/1', env_url)
            if is_production:
                send_message.assert_called_with({'message': 'message'}, mock.ANY, mock.ANY)
            else:
                send_message.assert_not_called()
        else:
            set_environment_external_url.assert_not_called()
            send_message.assert_not_called()

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_handle_deployment_pipeline_update_non_prod(self):
        """Test that non-prod pipelines are handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(is_production=False)

    @mock.patch('cki.deployment_tools.deployment_bot.deployment.IRC_BOT_EXCHANGE', 'exchange')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_handle_deployment_pipeline_update_prod(self):
        """Test that prod pipelines are handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(is_production=True)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_handle_deployment_pipeline_update_cancelled(self):
        """Test that a different job status is handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(
            is_production=False, status='cancelled', expected_result=False)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_handle_deployment_pipeline_update_no_env_url(self):
        """Test that missing env_url vars are handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(is_production=False, variables={
            'dummy': 'var',
        }, expected_result=False)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_handle_deployment_pipeline_update_direct_env_url(self):
        """Test that direct env urls are handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(is_production=False, variables={
            'deployment_bot_environment_url': 'https://instance/group/name/-/environments/1',
            'deployment_bot_external_url': 'direct-env-url',
            'deployment_bot_message_success': 'message',
        }, env_url='direct-env-url')

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_handle_deployment_pipeline_update_no_external_url(self):
        """Test that no env urls are handled correctly."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._handle_deployment_pipeline_update(is_production=False, variables={
            'deployment_bot_environment_url': 'https://instance/group/name/-/environments/1',
            'deployment_bot_external_url': '',
            'deployment_bot_message_success': 'message',
        }, env_url=None)

    @responses.activate
    def test_trigger_deployment_pipeline(self):
        """Test that pipeline triggering is handled correctly."""
        cases = (
            ('nonprod', False, {}, False),
            ('production', True, {'json': {'web_url': 'web-url'}}, True),
            ('no jobs', True, {'status': 400, 'json': {'base': [
             'Pipeline will not run for the selected trigger. '
             'The rules configuration prevented any jobs from being added to the pipeline.'
             ]}}, True),
        )
        for description, is_production, response, expected in cases:
            with (self.subTest(description),
                  mock.patch('cki_lib.misc.is_production', return_value=is_production)):
                api = 'https://deployment-instance/api/v4/projects'
                responses.reset()
                responses.add(responses.POST, f'{api}/project/pipeline', **response)
                config = config_tree.process_config_tree(PROJECT_CONFIG)
                deployment.Deployment(config).trigger_deployment_pipeline(
                    'https://instance/deployment',
                    config['irc-bot'], {'foo': 'baz'})
                if expected:
                    self.assertEqual(
                        responses.calls[0].request.body,
                        b'{"ref": "main", "variables": [{"key": "foo", "value": "baz"}]}')
                else:
                    self.assertEqual(len(responses.calls), 0)

    @staticmethod
    @responses.activate
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.'
                'trigger_deployment_pipeline')
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.'
                'handle_deployment_pipeline_update')
    def _callback(handle_deployment_pipeline_update, trigger_deployment_pipeline, *,
                  body=None, pipeline=False, message=False, deployment_pipeline=False):
        # pylint: disable=too-many-arguments
        api = 'https://source-project-url/api/v4/projects'
        responses.add(responses.GET,
                      f'{api}/group%2Fname/environments?states=available',
                      json=[{'id': 1}])
        responses.add(responses.GET,
                      f'{api}/group%2Fname/environments/1',
                      json={'id': 1, 'name': 'env', 'last_deployment': {'deployable': {'id': 1}}})
        responses.add(responses.GET,
                      f'{api}/group%2Fname/jobs/1',
                      json={'ref': 'main', 'commit': {'id': 2, 'title': 'commit title'}})
        responses.add(responses.GET,
                      f'{api}/group%2Fname/repository/commits/2/merge_requests',
                      json=[])

        handle_deployment_pipeline_update.return_value = deployment_pipeline
        config = config_tree.process_config_tree(PROJECT_CONFIG)
        deployment.Deployment(config).callback(body=body)
        if pipeline:
            handle_deployment_pipeline_update.assert_called_with(
                'https://deployment-instance/project',
                [1, 2],
                'success',
                {'foo': 'baz'})
            trigger_deployment_pipeline.assert_not_called()
        if message:
            handle_deployment_pipeline_update.assert_not_called()
            trigger_deployment_pipeline.assert_called_with(
                'https://source-project-url/group/name',
                config['irc-bot'], {
                    'JOB': '1',
                    'NAME': 'name',
                    'PROJECT_PATH': 'name',
                    'TAG': 'mr-123',
                    'FOO': 'baz',
                    'deployment_bot_message_success': 'group/name commit title env '
                    'https://source-project-url/group/name/-/jobs/1 ok',
                    'deployment_bot_message_failed': 'group/name commit title env '
                    'https://source-project-url/group/name/-/jobs/1 fail',
                    'deployment_bot_environment_url':
                    'https://source-project-url/group/name/-/environments/1'})

    def test_callback_message(self):
        """Test that the callbacks handles messages."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._callback(body={
            'repository': {'homepage': 'https://source-project-url/group/name'},
            'object_kind': 'build',
            'build_status': 'success',
            'build_id': 1,
            'environment': {
                'action': 'start',
                'name': 'mr-123',
            },
        }, message=True)

    def test_callback_pipeline(self):
        """Test that the callbacks handles pipelines."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._callback(body={
            'project': {'web_url': 'https://deployment-instance/project'},
            'object_kind': 'pipeline',
            'object_attributes': {'status': 'success',
                                  'variables': [{'key': 'foo', 'value': 'baz'}]},
            'builds': [{'id': 1}, {'id': 2}],
        }, pipeline=True, deployment_pipeline=False)

    def test_callback_pipeline_deployment(self):
        """Test that the callbacks handles deployment pipelines."""
        # pylint: disable=no-value-for-parameter  # mocks
        self._callback(body={
            'project': {'web_url': 'https://deployment-instance/project'},
            'object_kind': 'pipeline',
            'object_attributes': {'status': 'success',
                                  'variables': [{'key': 'foo', 'value': 'baz'}]},
            'builds': [{'id': 1}, {'id': 2}],
        }, pipeline=True, deployment_pipeline=True)


class TestMain(unittest.TestCase):
    """Test the main method."""

    @staticmethod
    @mock.patch.dict(os.environ, {'DEPLOYMENT_BOT_CONFIG':
                                  yaml.safe_dump(PROJECT_CONFIG)})
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.callback')
    def test_message(callback):
        """Check that messages can be passed manually."""
        __main__.main(['--message', '{"message":{"body":"contents"}}'])
        callback.assert_called_with(body={'message': {'body': 'contents'}})

    @staticmethod
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.callback')
    def test_config_path(callback):
        """Check that messages can be passed manually."""
        with tempfile.NamedTemporaryFile() as config:
            config.write(yaml.safe_dump(PROJECT_CONFIG).encode('utf8'))
            config.seek(0)
            with mock.patch.dict(os.environ, {'DEPLOYMENT_BOT_CONFIG_PATH': config.name}):
                __main__.main(['--message', '{"message":{"body":"contents"}}'])
        callback.assert_called_with(body={'message': {'body': 'contents'}})

    @staticmethod
    @mock.patch.dict(os.environ, {'DEPLOYMENT_BOT_CONFIG':
                                  yaml.safe_dump(PROJECT_CONFIG)})
    @mock.patch('cki.deployment_tools.deployment_bot.deployment.Deployment.listen')
    def test_listen(listen):
        """Check that the listener is called by default."""
        __main__.main([])
        listen.assert_called_with()
